# Introduction to Numerical Modelling
# Assignment 3
# Question 2

import numpy as np
import matplotlib.pyplot as plt

print ()
# Question 2
print ("Question 2: Evaluate the wind from the pressure gradient using \
a more accurate method of numerical differentiation.")
print ()

# Define the constants
pa = 1e5            # mean atmospheric pressure (Pa)
pb = 200            # pressure variations (Pa)
L = 2.4e6           # length scale of variations (m)
f = 1e-4            # coriolis parameter (rad/s)
r = 1               # density of air (kg/m^3)
ymin = 0            # y domain start (m)
ymax = 1e6          # y domain end (m)

# Size and resolution of the domain
N = 10                        # number of intervals
dy = (ymax - ymin)/N          # size of intervals (m)

# Give the domain a dimension y
y = np.linspace(ymin, ymax, N+1)

# Pressure across the domain
p = pa + pb*np.cos(y*np.pi/L)

# Pressure gradient across the domain
dpdy = -pb*(np.pi/L)*np.sin(y*np.pi/L)

# Geostrophic wind across the y domain
geowind = (-1/(r*f))*dpdy

# Numerical wind across the domain
# inital array of zeroes
numwind = np.zeros_like(y)
# wind for N=0 - 2nd order
numwind[0] = (-1/(r*f))*(-0.5*p[2]+2*p[1]-1.5*p[0])/dy
# wind for N=N - 2nd order
numwind[N] = (-1/(r*f))*(1.5*p[N]-2*p[N-1]+0.5*p[N-2])/dy
for i in range(1,N):
    # wind for N=1 or N=N-1 - 2nd order
    if i==1 or i==(N-1):
        numwind[i] = (-1/(r*f))*(p[i+1]-p[i-1])/(2*dy)
    # wind for 1<N<N-1 - 4th order
    else:
        numwind[i] = (-1/(r*f))*(p[i-2]-p[i+2] + 8*(p[i+1]-p[i-1]))/(12*dy)

# Plot geostrophic and numerical wind over the domain
plt.plot(y/1000, geowind, label="geostrophic")
plt.plot(y/1000, numwind, label="numerical")
plt.legend()
plt.title("Geostrophic wind vs 4th and 2nd order numerical wind")
plt.xlabel("y (km)")
plt.ylabel("wind speed (m/s)")
plt.xlim(0, 1000)
plt.ylim(0,2.6)
plt.tight_layout()
plt.show()

# Plot the errors in the numerical solution
plt.plot(y/1000, numwind-geowind)
plt.title("Error in 4th and 2nd order numerical wind vs geostrophic wind")
plt.xlabel("y (km)")
plt.ylabel("error in numerical wind speed (m/s)")
plt.xlim(0,1000)
plt.ylim(-0.1,0.2)
plt.tight_layout()
plt.show()

# Plot the errors in the numerical solution - crop out 1st order section
plt.plot(y/1000, numwind-geowind)
plt.title("Error in 4th order numerical wind vs geostrophic wind")
plt.xlabel("y (km)")
plt.ylabel("error in numerical wind speed (m/s)")
plt.xlim(0,1000)
plt.ylim(-0.02,0.02)
plt.tight_layout()
plt.show()

# Testing ideas

# index error
# print (numwind[12])

# asserion error
# assert numwind[2] > numwind[8]